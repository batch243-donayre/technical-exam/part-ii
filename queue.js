let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    
   
    let i = collection.length;
    collection[i] = element;

    return collection;
}


function dequeue() {
    // In here you are going to remove the firts element in the array

    const deleteFirstElement = [];

        if (collection.length !== 0) {
            for (let i = 1; i < collection.length; i++) {
                deleteFirstElement[i - 1] = collection[i]
            }
        }
        collection = deleteFirstElement

        return collection;
}

function front() {
    // In here, you are going to kukunin yung first element

    return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
    let collectionLength = 0;
        while (collection[collectionLength])
            collectionLength++;

        return collectionLength;  
}

function isEmpty() {
    //it will check whether the function is empty or not
    if (collection !== 0) {
            return false;
    }else{
        return true;
    }       
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};